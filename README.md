
IMPORTANT We use git lfs to deal with heavy images, please install it before cloning the repository:

    sudo apt-get install git-lfs
    git lfs install


Hel is a game produced according to VideA game principles: 

Hela scenario is inspired by the story of the Henrietta Lacks whose cells gave birth to the "immortal cell line" HeLa which originated from cancer stem cells taken from the pelvis of Henrietta Lacks without her knowledge in 1957 and has been used throughout the world. It is the most commonly used cell line, it has led to research successes on many diseases. If Henrietta Lacks was not cured and died, her cells took on an autonomous life, they were cloned in a massive way, even transformed for the study of cancers of other organs. HeLa cells are known to have contaminated other cells in laboratories, which is what finally made the family of the deceased Henrietta Lacks aware of the existence of HeLa, bringing a form of posthumous recognition. Several forms of tributes have been paid, including an asteroid named after her 359426Lacks, the naming was based on a proposal by Carrie Nugent who also proposed to name an asteroid after Rosa Parks. It is on this Asteroid that the storyline of our game takes place, it addresses the wonderful story of this immortal cell line from a black woman who was not healed but helped heal many whites.
Aesthetics

Dynamic backgrounds and procedural (generative) scenery composed of HeLa cells, so that each time the game is launched, just as the HeLa cells split infinitely, the environment in which the player moves is never exactly the same as the previous game.
The shapes evoke living organisms in constant mutation (in the design of the characters as well as in the elements of the scenery) inspired by images of cells observed under the microscope

The game is developed following in a feminist group of women identified persons a horizontal and collaborative model under a free art licence, and using the free software Godot Engine. Please feel free to check our source code and eventually reach out to us if you are interested in contributing or hosting workshops where we present our organisation and work procedure.

All workshops and events are announced on https://ps.zoethical.org/c/engagement/jeu-vide-a/116 and you can reach out to or @ameliedumont or @natacha regarding any questions or propositions.

