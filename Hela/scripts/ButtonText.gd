extends Button

onready var vp_size:Vector2 = get_viewport().size
var resize:bool = true

#func vp_changed():
#	vp_size = get_viewport().size
#	resize = true

func _ready():
#	get_viewport().connect("size_changed",self,"vp_changed")
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://gui/fabrik.ttf")
	dynamic_font.size = 50
	set("custom_fonts/font",dynamic_font)
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = Color(1,0.4,0.7,1)
	norm_styl.content_margin_top = 12
	norm_styl.content_margin_right = 20
	norm_styl.content_margin_bottom = 12
	norm_styl.content_margin_left = 20
	norm_styl.set_corner_radius_all(100)
	norm_styl.set_shadow_color(Color(0.6,0.7,0.8,1))
	norm_styl.set_shadow_size(8)
	set("custom_styles/normal",norm_styl)
	var hover_styl:StyleBoxFlat = StyleBoxFlat.new()
	hover_styl.bg_color = Color(0.5,1,0.7,1)
	hover_styl.content_margin_top = 12
	hover_styl.content_margin_right = 20
	hover_styl.content_margin_bottom = 12
	hover_styl.content_margin_left = 20
	hover_styl.set_corner_radius_all(100)
	hover_styl.set_shadow_color(Color(1,0.4,0.5,1))
	hover_styl.set_shadow_size(8)
	set("custom_styles/hover",hover_styl)
	var click_styl:StyleBoxFlat = StyleBoxFlat.new()
	click_styl.bg_color = Color(0.5,1,0.7,1)
	click_styl.content_margin_top = 12
	click_styl.content_margin_right = 20
	click_styl.content_margin_bottom = 12
	click_styl.content_margin_left = 20
	click_styl.set_corner_radius_all(100)
	click_styl.set_shadow_color(Color(1,0.4,0.5,1))
	click_styl.set_shadow_size(8)
	rect_size.x = vp_size.x/6
	rect_size.y = vp_size.y/6
	rect_position.x = vp_size.x-rect_size.x-20
	rect_position.y = vp_size.y-rect_size.y-20

#func _process(delta):
#	if resize:
#		rect_size.x = vp_size.x/8
#		rect_size.y = vp_size.y/6
#		rect_position.x = (vp_size.x/2-rect_size.x)-20
#		rect_position.y = (vp_size.y/2-rect_size.y)-20
#		resize = false
	#rect_size.x = vp_size.x/8
	#rect_size.y = vp_size.y/6
	#rect_position.x = (vp_size.x/2-rect_size.x)-20
	#rect_position.y = (vp_size.y/2-rect_size.y)-20
	
