extends Node2D

onready var vp_size:Vector2 = get_viewport().size
onready var players = [$BluePlayer, $GreenPlayer, $OrangePlayer, $YellowPlayer, $PinkPlayer]
var resize:bool = true
var newLabel = preload("res://scenes/Dialog.tscn")
var newButton = preload("res://scenes/ButtonText.tscn")
var HelaLabel = preload("res://scenes/HelaDialog.tscn")
var LGoal = preload("res://scenes/LevelGoal.tscn")
var switch_cameras = true
var active_player
var question_to_hela = ""
var hela_answer = ""
var question_from_hela = false
var answer_to_hela = false
var call_hela = false
var hela_called = false
var player1
var player2

func _ready():
	OS.set_window_maximized(true)
	Global.hela_help = false
	Global.hela_callable = true
	Global.level_goal = "Find more about HeLa's history"
	active_player = $BluePlayer
	player1 = $BluePlayer
	player2 = $GreenPlayer
	$GreenPlayer.health = 75
	$Camera2D.make_current()
	$Camera2D.position = active_player.position
	$CanvasLayer/ButtonText.visible = false
	$HeLa.visible = false
	check_active_player()
	
func _process(delta):
	check_active_player()
	print($CanvasLayer.get_children())
#	if hela_called:
#		$CanvasLayer.scale = Vector2(2,2)
#		$CanvasLayer/GUI.rect_position.x = -get_viewport().get_canvas_transform()[2].x + (get_viewport().size.x)/2
#		$CanvasLayer/GUI.rect_position.y = -get_viewport().get_canvas_transform()[2].y + (get_viewport().size.y)/2
#		$CanvasLayer/ButtonHela.rect_position.x = -get_viewport().get_canvas_transform()[2].x + (get_viewport().size.x)/2 + 200
#		$CanvasLayer/ButtonHela.rect_position.y = -get_viewport().get_canvas_transform()[2].y + $CanvasLayer/ButtonHela.rect_size.y - 54
#		$CanvasLayer/ButtonText.rect_position.x = -get_viewport().get_canvas_transform()[2].x  + get_viewport().size.x - $CanvasLayer/ButtonText.rect_size.x - 50
#		$CanvasLayer/ButtonText.rect_position.y = -get_viewport().get_canvas_transform()[2].y  + get_viewport().size.y - $CanvasLayer/ButtonText.rect_size.y - 50
#	else :
#		$CanvasLayer.scale = Vector2(1,1)
#		$CanvasLayer/GUI.rect_position.x = -get_viewport().get_canvas_transform()[2].x + (get_viewport().size.x)/2
#		$CanvasLayer/GUI.rect_position.y = -get_viewport().get_canvas_transform()[2].y + (get_viewport().size.y)/2
#		$CanvasLayer/ButtonHela.rect_position.x = -get_viewport().get_canvas_transform()[2].x + (get_viewport().size.x)/2 + 200
#		$CanvasLayer/ButtonHela.rect_position.y = -get_viewport().get_canvas_transform()[2].y + $CanvasLayer/ButtonHela.rect_size.y - 54
#		$CanvasLayer/ButtonText.rect_position.x = -get_viewport().get_canvas_transform()[2].x  + get_viewport().size.x - $CanvasLayer/ButtonText.rect_size.x - 50
#		$CanvasLayer/ButtonText.rect_position.y = -get_viewport().get_canvas_transform()[2].y  + get_viewport().size.y - $CanvasLayer/ButtonText.rect_size.y - 50
	if Input.is_action_just_pressed("ui_select"):
		if switch_cameras:
			if active_player == player1:
				active_player = player2
			elif active_player == player2:
				active_player = player1
			$Camera2D.position = active_player.position
			
	if len($CanvasLayer.get_children()) > 4: 
#		var label = $CanvasLayer.get_child(4)
#		label.rect_position.x = -get_viewport().get_canvas_transform()[2].x + (get_viewport().size.x)/2
#		label.rect_position.y = -get_viewport().get_canvas_transform()[2].y + (get_viewport().size.y)/2
		var label_answer = newLabel.instance()
		var helalabel = HelaLabel.instance()
		
		if question_from_hela: #1e question de HeLa
			if $CanvasLayer.get_child(4).dialog_complete: #suppression de la question de Hela
				$CanvasLayer.remove_child($CanvasLayer.get_child(4))
				label_answer.dialog = ["Yes. Indeed we would like to learn more about you.", "Could you please tell us how is it possible that your cells can produce so many effects?", "What did your cells accomplish before?"]
				label_answer.speaker = "Player"
				$CanvasLayer.add_child(label_answer)
				question_from_hela = false
				answer_to_hela = true
		if answer_to_hela: #reponse joueurs
			if $CanvasLayer.get_child(4).dialog_complete:
				$CanvasLayer.remove_child($CanvasLayer.get_child(4))
				helalabel.dialog = ["Well this is a long story and it implies some violence in it. Are you sure you want to know?"]
				$CanvasLayer.add_child(helalabel)
				answer_to_hela = false
				
				
		if $CanvasLayer.get_child(4).dialog_complete:
			$CanvasLayer.remove_child($CanvasLayer.get_child(4))
			if Global.hela_help:
				#$CanvasLayer.remove_child(get_child(4))
				if question_to_hela == "rock":
					helalabel.dialog = ["Sure. I grant you some time for making your environment more friendly"]
					hela_answer = "rock"
					question_to_hela = ""
					Global.hela_help = false
				elif question_to_hela == "platform":
					helalabel.dialog = ["Let's adapt your bodies to the space"]
					hela_answer = "platform"
					question_to_hela = ""
					Global.hela_help = false
				elif question_to_hela == "tunnel":
					helalabel.dialog = ["I guess I can open the space a little"]
					hela_answer = "tunnel"
					question_to_hela = ""
					Global.hela_help = false
				elif question_to_hela == "cells":
					helalabel.dialog = ["What if I make my cells a little more active?"]
					hela_answer = "cells"
					question_to_hela = ""
					Global.hela_help = false
				else:
					helalabel.dialog = ["I am afraid there is not much to be modified here"]
					Global.hela_help = false
				$CanvasLayer.add_child(helalabel)
			else:
				Global.hela_callable = true
				$CanvasLayer.remove_child($CanvasLayer.get_child(4))
				hela_called = false
				switch_cameras = true
				call_hela = false
				for p in players:
					p.can_move = true
					
func _input(event):
	if event is InputEventMouseButton && event.is_pressed():
#		event.position.x += -get_viewport().get_canvas_transform()[2].x
#		event.position.y += -get_viewport().get_canvas_transform()[2].y
		if Global.switch_players:
			for p in players:
				if p.mouse_in:
					match event.button_index:
						BUTTON_LEFT:
							player1 = p
							active_player = player1
						BUTTON_RIGHT:
							player2 = p
							active_player = player2
			Global.switch_players = false
		else:
			pass
	
func check_active_player():
	if hela_called:
		$Camera2D.position = Vector2(-1043, -1128)
		$Camera2D.zoom = Vector2(2,2)
	else:
		for p in players:
			if active_player == p:
				p.is_active = true
			else:
				p.is_active = false
			if player1 == p:
				p.is_player1 = true
				p.is_player2 = false
			elif player2 == p:
				p.is_player1 = false
				p.is_player2 = true
			else: 
				p.is_player1 = false
				p.is_player2 = false
			if Global.hela_callable:
				$CanvasLayer/ButtonHela.visible = true
			else:
				$CanvasLayer/ButtonHela.visible = false
		$Camera2D.position = active_player.position
		$Camera2D.zoom = Vector2(1,1)

func _on_HelaLake_body_entered(body):
	call_hela = true
	$CanvasLayer/ButtonText.text = "CALL FOR HELA"
	$CanvasLayer/ButtonText.visible = true

func _on_ButtonText_pressed():
	if call_hela:
		hela_called = true
		$HeLa.visible = true
		$HeLa.play("apparition")
		var lake_shader = $Lake.material
		lake_shader.set_shader_param("deformation_speed", 0.02)
		lake_shader.set_shader_param("deformation_size", Vector2(0.01, 0.01))

func _on_HelaLake_body_exited(body):
	call_hela = false
	$CanvasLayer/ButtonText.visible = false

func _on_ButtonHela_pressed():
	Global.hela_help = true
	var label = newLabel.instance()
	switch_cameras = false
	for p in players:
		p.can_move = false
	label.dialog = ["HeLa, could you help us facing this situation?"]
	label.speaker = "Player"
	$CanvasLayer.add_child(label)

func _on_HeLa_animation_finished():
	var anim = $HeLa.get_animation()
	if anim == "apparition":
		$HeLa.play("normal")
		var label = HelaLabel.instance()
		switch_cameras = false
		for p in players:
			p.can_move = true
		label.dialog = ["Oh hello again! How are you? Is there something I can do for you?"]
		$CanvasLayer.add_child(label)
		question_from_hela = true
	else:
		pass
