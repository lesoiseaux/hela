extends Button

onready var vp_size:Vector2 = get_viewport().size
var resize:bool = true

func vp_changed():
	vp_size = get_viewport().size
	resize = true

func _ready():
	get_viewport().connect("size_changed",self,"vp_changed")
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://gui/fabrik.ttf")
	dynamic_font.size = 30
	set("custom_fonts/font",dynamic_font)
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = Color(0.1,0.7,1,1)
	norm_styl.content_margin_top = 12
	norm_styl.content_margin_right = 12
	norm_styl.content_margin_bottom = 12
	norm_styl.content_margin_left = 12
	norm_styl.set_shadow_color(Color(1,0.4,0.5,1))
	norm_styl.set_shadow_size(8)
	norm_styl.set_corner_radius_all(8)
	set("custom_styles/normal",norm_styl)
	var hover_styl:StyleBoxFlat = StyleBoxFlat.new()
	hover_styl.bg_color = Color(0.5,1,0.7,1)
	hover_styl.content_margin_top = 12
	hover_styl.content_margin_right = 12
	hover_styl.content_margin_bottom = 12
	hover_styl.content_margin_left = 12
	hover_styl.set_shadow_color(Color(1,0.4,0.5,1))
	hover_styl.set_shadow_size(8)
	hover_styl.set_corner_radius_all(8)
	set("custom_styles/hover",hover_styl)
	rect_size.x = vp_size.x/15
	rect_size.y = 81
	rect_position.x = vp_size.x+rect_size.x - 70
	rect_position.y = 25

func _process(delta):
	if Global.hela_callable:
		visible = true
	else:
		visible = false
	
