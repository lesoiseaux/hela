extends Label

onready var vp_size:Vector2 = get_viewport().size
var resize:bool = true

func vp_changed():
	vp_size = get_viewport().size
	resize = true

func _ready():
	get_viewport().connect("size_changed",self,"vp_changed")
	var dynamic_font = DynamicFont.new()
	dynamic_font.font_data = load("res://gui/fabrik.ttf")
	dynamic_font.size = 30
	set("custom_fonts/font",dynamic_font)
	rect_size.x = vp_size.x/8
	rect_size.y = vp_size.y/6
	rect_position.x = (vp_size.x/2-rect_size.x)-20
	rect_position.y = -vp_size.y/2+120


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if resize:
		rect_size.x = vp_size.x/8
		rect_size.y = vp_size.y/6
		rect_position.x = (vp_size.x/2-rect_size.x)-20
		rect_position.y = -vp_size.y/2+120
		resize = false
	#rect_size.x = vp_size.x/8
	#rect_size.y = vp_size.y/6
	#rect_position.x = (vp_size.x/2-rect_size.x)-20
	#rect_position.y = -vp_size.y/2+120
