extends Control

onready var txt = $Loading
onready var sprite = $AnimatedSprite
onready var bg = $Bg

onready var vp_size:Vector2 = get_viewport().size
var resize:bool = true

func vp_changed():
	vp_size = get_viewport().size
	resize = true

func _ready():
	get_viewport().connect("size_changed",self,"vp_changed")
	rect_position.x = -vp_size.x/2
	rect_position.y = -vp_size.y/2
	sprite.position.x = vp_size.x/2 - 250
	sprite.position.y = vp_size.y/2 - 100
	txt.rect_position.x = vp_size.x/2 - txt.rect_size.x/2
	txt.rect_position.y = vp_size.y/2 - txt.rect_size.y/2

func _process(delta):
	rect_position.x = -vp_size.x/2
	rect_position.y = -vp_size.y/2
	sprite.position.x = vp_size.x/2 - 250
	sprite.position.y = vp_size.y/2 - 100
	txt.rect_position.x = vp_size.x/2 - txt.rect_size.x/2
	txt.rect_position.y = vp_size.y/2 - txt.rect_size.y/2
