shader_type canvas_item;

uniform sampler2D noise1 : hint_black;
uniform sampler2D noise2 : hint_black;

uniform vec2 size = vec2(1.0, 1.0);
uniform vec2 scale = vec2(1.0, 1.0);
uniform vec2 tile_factor = vec2(1.0, 1.0);

uniform float deformation_speed = 0.1;
uniform vec2 deformation_size = vec2(0.01, 0.01);
uniform vec4 color: hint_color = vec4(0.0, 0.0, 1.0, 0.0);
uniform float water_level = 40.0;
uniform float surface_width = 5.0;

uniform float wave_speed = 50.0;
uniform float wave_length = 10.0;
uniform float wave_height = 1.0;
uniform float wave_distance_attenuation = 1.0;
uniform float wave_time_attenuation = 1.0;
uniform float impact_time = 1.0;
uniform float current_time = 1.0;

float add_wave_impact(vec2 pixel_pos, vec2 impact_pixel_pos, float time_since_impact) {
	float dist = length(pixel_pos - impact_pixel_pos);
	float delay = dist / wave_speed;
	
	if(time_since_impact < delay) {
		return 0.0;
	}
	
	float amp = sin(wave_speed * (time_since_impact - delay) / wave_length);
	amp /= 1.0 + dist * wave_distance_attenuation * 0.01;
	amp *= exp(-time_since_impact * wave_time_attenuation);
	return amp * wave_height;
}

void fragment() {
	vec2 u_pos = UV * scale * tile_factor + TIME * deformation_speed;
	vec2 offset = vec2(texture(noise1, u_pos).x, texture(noise2, u_pos).y) - 0.5;
	vec2 pixel_pos = UV * scale * size;
	offset += add_wave_impact(pixel_pos, vec2(200.0, 40.0), TIME -impact_time);
	vec2 deformation = offset * deformation_size;
	vec2 real_pos = (UV + deformation * 10.0) * scale * size;
	if(real_pos.y < water_level){
		COLOR = vec4(0.3, 0.2, 0.5, 1.0);
	} else if(real_pos.y < water_level + surface_width) {
		COLOR = vec4(1.0, 1.0, 1.0, 1.0);
	} else {
		COLOR = mix(textureLod(SCREEN_TEXTURE, SCREEN_UV + deformation, 0.0), vec4(color.rgb, 1.0), color.a);
	}
	
}